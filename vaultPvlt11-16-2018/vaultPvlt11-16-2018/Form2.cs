﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace vaultPvlt11_16_2018
{
    public partial class Form2 : Form
    {
        #region form initialize
        public Form2() //initializes form and removes controls
        {
            InitializeComponent();
            ControlBox = false;
        }
        #endregion

        #region print line method
        public void PrintLinePvlt(string functionStringPvlt) //prints text on monitor according to pressed input
        {
            try
            {
                switch (functionStringPvlt)
                {
                    case "btnOnePvlt":
                        rbxSerialMonitorPvlt.AppendText("Input 1\n");
                        break;

                    case "btnTwoPvlt":
                        rbxSerialMonitorPvlt.AppendText("Input 2\n");
                        break;

                    case "btnThreePvlt":
                        rbxSerialMonitorPvlt.AppendText("input 3\n");
                        break;

                    case "btnAboutPvlt":
                        rbxSerialMonitorPvlt.AppendText("About button was clicked\n");
                        break;
                }
            }

            catch
            {
                return;
            }
        }
        #endregion

        #region print code method
        public void PrintCodePvlt(string codeStringPvlt) //prints code according to combined input
        {
            try
            {
                rbxSerialMonitorPvlt.AppendText("The code is " + codeStringPvlt + "\n");
            }
            
            catch
            {
                return;
            }
        }
        #endregion

        #region print result method
        public void PrintResultPvlt(string combResultPvlt) //prints result message after code check
        {
            try
            {
                if (combResultPvlt == "Correct")
                {
                    rbxSerialMonitorPvlt.AppendText("Correct combination. Granting access to empty bank account.\n");
                }

                else
                {
                    rbxSerialMonitorPvlt.AppendText("Wrong combination. The vault has been locked.\n");
                }
            }

            catch
            {
                return;
            }
        }
        #endregion

        #region reset line
        public void ResetLinePvlt(string reset) //prints application reset message
        {
            try
            {
                rbxSerialMonitorPvlt.AppendText("Application reset\n");
            }

            catch
            {
                return;
            }
        }
        #endregion
    }
}
