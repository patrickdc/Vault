﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace vaultPvlt11_16_2018
{
    public partial class Form1 : Form
    {
        #region variables
        Form2 serialMonitorFormPvlt = new Form2();

        int buttonsPressedPvlt = 0;
        Random vaultCodeRandomPvlt = new Random(); //random nr tussen 1 en 3!
        string vaultCodePvlt = "";
        string guessedCodePvlt = "";
        string inputCodePvlt = "";
        int wrongCombAttemptsPvlt = 0;
        int defaultHeightPvlt = 306;
        int timerPvlt = 0;
        #endregion

        #region forminitialize
        public Form1() //form initialize, sets default height and puts vaultcode value in the cheat code label
        {
            InitializeComponent();
            lblCheatCodeValuePvlt.Text = vaultCodePvlt;
            this.Height = defaultHeightPvlt;
        }
        #endregion

        #region all buttons clicked event
        private void btnAllPvlt_Click(object sender, EventArgs e) //sends input to the serial monitor and guessed code, checks if code matches, checks buttons pressed, sends result to vault and serialmonitor
        {

            switch ((sender as Button).Name)
            {
                case "btnOnePvlt":
                    inputCodePvlt = "1";
                break;

                case "btnTwoPvlt":
                    inputCodePvlt = "2";
                break;

                case "btnThreePvlt":
                    inputCodePvlt = "3";
                break;
            }

            if (buttonsPressedPvlt == 0)
            {
                ResetApp();
            }

            buttonsPressedPvlt++;

            lblButtonsPressedValuePvlt.Text = Convert.ToString (buttonsPressedPvlt);

            guessedCodePvlt = guessedCodePvlt + inputCodePvlt;

            serialMonitorFormPvlt.PrintLinePvlt(((Button)sender).Name);
            serialMonitorFormPvlt.PrintCodePvlt(guessedCodePvlt);

            if (buttonsPressedPvlt == 3)
            {
                if (guessedCodePvlt == vaultCodePvlt)
                {
                    pbxGreenPvlt.BackColor = Color.Green;
                    buttonsPressedPvlt = 0;
                    serialMonitorFormPvlt.PrintResultPvlt("Correct");//
                    chbxCheckboxPvlt.Checked = true;//
                }

                else
                {
                    pbxRedPvlt.BackColor = Color.Red;
                    buttonsPressedPvlt = 0;
                    serialMonitorFormPvlt.PrintResultPvlt("Wrong");
                    chbxCheckboxPvlt.Checked = false;
                    wrongCombAttemptsPvlt++;

                    if (wrongCombAttemptsPvlt >= 3)
                    {
                        MessageBox.Show("You've tried to unlock the vault too many times in a short period. \nStill drunk? \nNow you have to wait.");
                        wrongCombAttemptsPvlt = 0;
                        tmrTimerPvlt.Start();
                    }
                }
            }
        }
        #endregion

        #region reset app method
        private void ResetApp() //method which resets colors, labels and variables to default
        {
            guessedCodePvlt = "";
            pbxGreenPvlt.BackColor = Color.Empty;
            pbxRedPvlt.BackColor = Color.Empty;
            chbxCheckboxPvlt.Checked = false;
            buttonsPressedPvlt = 0;
        }
        #endregion

        #region serialbox checked
        private void chbxSerialPvlt_CheckedChanged(object sender, EventArgs e) //shows serial monitor if checkbox is checked
        {
            try
            {
                if (chbxSerialPvlt.Checked == true)
                {
                    serialMonitorFormPvlt.Show();
                }
                else
                {
                    serialMonitorFormPvlt.Hide();
                }
            }

            catch
            {
                return;
            }
        }
        #endregion

        #region about button clicked event
        private void btnAboutPvlt_Click(object sender, EventArgs e) //shows message about the application
        {
            serialMonitorFormPvlt.PrintLinePvlt(((Button)sender).Name);
            MessageBox.Show("This application was made by \n \tPatrick Veltrop");
        }
        #endregion

        #region reset button clicked event
        private void btnResetPvlt_Click(object sender, EventArgs e) //resets the application
        {
            serialMonitorFormPvlt.ResetLinePvlt("reset");
            ResetApp();
        }
        #endregion

        #region cheatbox checked event
        private void ckbCheatPvlt_CheckedChanged(object sender, EventArgs e) //resizes app window to show cheat label
        {
            if (ckbCheatPvlt.Checked == true)
            {
                this.Height = 367;
            }

            else
            {
                this.Height = defaultHeightPvlt;
            }
        }
        #endregion

        #region timer
        private void tmrTimerPvlt_Tick(object sender, EventArgs e) //timer which fires at too many wrong attempts, locks input
        {
            timerPvlt++;
            lblTimerPvlt.Text = Convert.ToString (timerPvlt);

            btnOnePvlt.Enabled = false;
            btnTwoPvlt.Enabled = false;
            btnThreePvlt.Enabled = false;
            btnResetPvlt.Enabled = false;

            if (timerPvlt == 10)
            {
                tmrTimerPvlt.Stop();
                timerPvlt = 0;
                btnOnePvlt.Enabled = true;
                btnTwoPvlt.Enabled = true;
                btnThreePvlt.Enabled = true;
                btnResetPvlt.Enabled = true;
            }
        }
        #endregion
    }
}
