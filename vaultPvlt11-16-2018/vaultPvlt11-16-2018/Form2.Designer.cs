﻿namespace vaultPvlt11_16_2018
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form2));
            this.rbxSerialMonitorPvlt = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // rbxSerialMonitorPvlt
            // 
            this.rbxSerialMonitorPvlt.BackColor = System.Drawing.SystemColors.InfoText;
            this.rbxSerialMonitorPvlt.ForeColor = System.Drawing.SystemColors.Window;
            this.rbxSerialMonitorPvlt.Location = new System.Drawing.Point(13, 13);
            this.rbxSerialMonitorPvlt.Name = "rbxSerialMonitorPvlt";
            this.rbxSerialMonitorPvlt.Size = new System.Drawing.Size(343, 211);
            this.rbxSerialMonitorPvlt.TabIndex = 0;
            this.rbxSerialMonitorPvlt.Text = "";
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(368, 236);
            this.Controls.Add(this.rbxSerialMonitorPvlt);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form2";
            this.Text = "Serial Monitor";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RichTextBox rbxSerialMonitorPvlt;
    }
}