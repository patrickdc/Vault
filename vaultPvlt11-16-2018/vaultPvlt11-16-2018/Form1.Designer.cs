﻿namespace vaultPvlt11_16_2018
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.grpbxOnePvlt = new System.Windows.Forms.GroupBox();
            this.btnThreePvlt = new System.Windows.Forms.Button();
            this.btnTwoPvlt = new System.Windows.Forms.Button();
            this.btnOnePvlt = new System.Windows.Forms.Button();
            this.grpbxTwoPvlt = new System.Windows.Forms.GroupBox();
            this.pbxGreenPvlt = new System.Windows.Forms.PictureBox();
            this.pbxRedPvlt = new System.Windows.Forms.PictureBox();
            this.grpbxThreePvlt = new System.Windows.Forms.GroupBox();
            this.ckbCheatPvlt = new System.Windows.Forms.CheckBox();
            this.chbxCheckboxPvlt = new System.Windows.Forms.CheckBox();
            this.chbxSerialPvlt = new System.Windows.Forms.CheckBox();
            this.btnAboutPvlt = new System.Windows.Forms.Button();
            this.grpbxFourPvlt = new System.Windows.Forms.GroupBox();
            this.btnResetPvlt = new System.Windows.Forms.Button();
            this.grpbxFivePvlt = new System.Windows.Forms.GroupBox();
            this.lblCheatCodePvlt = new System.Windows.Forms.Label();
            this.lblCheatCodeValuePvlt = new System.Windows.Forms.Label();
            this.grpbxSixPvlt = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblTimerPvlt = new System.Windows.Forms.Label();
            this.lblButtonsPressedPvlt = new System.Windows.Forms.Label();
            this.lblButtonsPressedValuePvlt = new System.Windows.Forms.Label();
            this.tmrTimerPvlt = new System.Windows.Forms.Timer(this.components);
            this.grpbxOnePvlt.SuspendLayout();
            this.grpbxTwoPvlt.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxGreenPvlt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxRedPvlt)).BeginInit();
            this.grpbxThreePvlt.SuspendLayout();
            this.grpbxFourPvlt.SuspendLayout();
            this.grpbxFivePvlt.SuspendLayout();
            this.grpbxSixPvlt.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpbxOnePvlt
            // 
            this.grpbxOnePvlt.Controls.Add(this.btnThreePvlt);
            this.grpbxOnePvlt.Controls.Add(this.btnTwoPvlt);
            this.grpbxOnePvlt.Controls.Add(this.btnOnePvlt);
            this.grpbxOnePvlt.Location = new System.Drawing.Point(13, 13);
            this.grpbxOnePvlt.Name = "grpbxOnePvlt";
            this.grpbxOnePvlt.Size = new System.Drawing.Size(162, 72);
            this.grpbxOnePvlt.TabIndex = 0;
            this.grpbxOnePvlt.TabStop = false;
            this.grpbxOnePvlt.Text = "Enter Keys";
            // 
            // btnThreePvlt
            // 
            this.btnThreePvlt.Location = new System.Drawing.Point(108, 19);
            this.btnThreePvlt.Name = "btnThreePvlt";
            this.btnThreePvlt.Size = new System.Drawing.Size(45, 42);
            this.btnThreePvlt.TabIndex = 2;
            this.btnThreePvlt.Text = "3";
            this.btnThreePvlt.UseVisualStyleBackColor = true;
            this.btnThreePvlt.Click += new System.EventHandler(this.btnAllPvlt_Click);
            // 
            // btnTwoPvlt
            // 
            this.btnTwoPvlt.Location = new System.Drawing.Point(57, 19);
            this.btnTwoPvlt.Name = "btnTwoPvlt";
            this.btnTwoPvlt.Size = new System.Drawing.Size(45, 42);
            this.btnTwoPvlt.TabIndex = 1;
            this.btnTwoPvlt.Text = "2";
            this.btnTwoPvlt.UseVisualStyleBackColor = true;
            this.btnTwoPvlt.Click += new System.EventHandler(this.btnAllPvlt_Click);
            // 
            // btnOnePvlt
            // 
            this.btnOnePvlt.Location = new System.Drawing.Point(6, 19);
            this.btnOnePvlt.Name = "btnOnePvlt";
            this.btnOnePvlt.Size = new System.Drawing.Size(45, 42);
            this.btnOnePvlt.TabIndex = 0;
            this.btnOnePvlt.Text = "1";
            this.btnOnePvlt.UseVisualStyleBackColor = true;
            this.btnOnePvlt.Click += new System.EventHandler(this.btnAllPvlt_Click);
            // 
            // grpbxTwoPvlt
            // 
            this.grpbxTwoPvlt.Controls.Add(this.pbxGreenPvlt);
            this.grpbxTwoPvlt.Controls.Add(this.pbxRedPvlt);
            this.grpbxTwoPvlt.Location = new System.Drawing.Point(181, 13);
            this.grpbxTwoPvlt.Name = "grpbxTwoPvlt";
            this.grpbxTwoPvlt.Size = new System.Drawing.Size(122, 72);
            this.grpbxTwoPvlt.TabIndex = 1;
            this.grpbxTwoPvlt.TabStop = false;
            this.grpbxTwoPvlt.Text = "Lock / Unlock";
            // 
            // pbxGreenPvlt
            // 
            this.pbxGreenPvlt.Location = new System.Drawing.Point(63, 19);
            this.pbxGreenPvlt.Name = "pbxGreenPvlt";
            this.pbxGreenPvlt.Size = new System.Drawing.Size(50, 41);
            this.pbxGreenPvlt.TabIndex = 1;
            this.pbxGreenPvlt.TabStop = false;
            // 
            // pbxRedPvlt
            // 
            this.pbxRedPvlt.Location = new System.Drawing.Point(7, 19);
            this.pbxRedPvlt.Name = "pbxRedPvlt";
            this.pbxRedPvlt.Size = new System.Drawing.Size(50, 41);
            this.pbxRedPvlt.TabIndex = 0;
            this.pbxRedPvlt.TabStop = false;
            // 
            // grpbxThreePvlt
            // 
            this.grpbxThreePvlt.Controls.Add(this.ckbCheatPvlt);
            this.grpbxThreePvlt.Controls.Add(this.chbxCheckboxPvlt);
            this.grpbxThreePvlt.Controls.Add(this.chbxSerialPvlt);
            this.grpbxThreePvlt.Location = new System.Drawing.Point(13, 149);
            this.grpbxThreePvlt.Name = "grpbxThreePvlt";
            this.grpbxThreePvlt.Size = new System.Drawing.Size(290, 52);
            this.grpbxThreePvlt.TabIndex = 2;
            this.grpbxThreePvlt.TabStop = false;
            this.grpbxThreePvlt.Text = "Tools";
            // 
            // ckbCheatPvlt
            // 
            this.ckbCheatPvlt.AutoSize = true;
            this.ckbCheatPvlt.Location = new System.Drawing.Point(104, 20);
            this.ckbCheatPvlt.Name = "ckbCheatPvlt";
            this.ckbCheatPvlt.Size = new System.Drawing.Size(54, 17);
            this.ckbCheatPvlt.TabIndex = 3;
            this.ckbCheatPvlt.Text = "Cheat";
            this.ckbCheatPvlt.UseVisualStyleBackColor = true;
            this.ckbCheatPvlt.CheckedChanged += new System.EventHandler(this.ckbCheatPvlt_CheckedChanged);
            // 
            // chbxCheckboxPvlt
            // 
            this.chbxCheckboxPvlt.AutoSize = true;
            this.chbxCheckboxPvlt.Enabled = false;
            this.chbxCheckboxPvlt.Location = new System.Drawing.Point(266, 21);
            this.chbxCheckboxPvlt.Name = "chbxCheckboxPvlt";
            this.chbxCheckboxPvlt.Size = new System.Drawing.Size(15, 14);
            this.chbxCheckboxPvlt.TabIndex = 2;
            this.chbxCheckboxPvlt.UseVisualStyleBackColor = true;
            // 
            // chbxSerialPvlt
            // 
            this.chbxSerialPvlt.AutoSize = true;
            this.chbxSerialPvlt.Location = new System.Drawing.Point(7, 20);
            this.chbxSerialPvlt.Name = "chbxSerialPvlt";
            this.chbxSerialPvlt.Size = new System.Drawing.Size(90, 17);
            this.chbxSerialPvlt.TabIndex = 0;
            this.chbxSerialPvlt.Text = "Serial Monitor";
            this.chbxSerialPvlt.UseVisualStyleBackColor = true;
            this.chbxSerialPvlt.CheckedChanged += new System.EventHandler(this.chbxSerialPvlt_CheckedChanged);
            // 
            // btnAboutPvlt
            // 
            this.btnAboutPvlt.Location = new System.Drawing.Point(7, 16);
            this.btnAboutPvlt.Name = "btnAboutPvlt";
            this.btnAboutPvlt.Size = new System.Drawing.Size(51, 23);
            this.btnAboutPvlt.TabIndex = 1;
            this.btnAboutPvlt.Text = "About";
            this.btnAboutPvlt.UseVisualStyleBackColor = true;
            this.btnAboutPvlt.Click += new System.EventHandler(this.btnAboutPvlt_Click);
            // 
            // grpbxFourPvlt
            // 
            this.grpbxFourPvlt.Controls.Add(this.btnResetPvlt);
            this.grpbxFourPvlt.Controls.Add(this.btnAboutPvlt);
            this.grpbxFourPvlt.Location = new System.Drawing.Point(13, 207);
            this.grpbxFourPvlt.Name = "grpbxFourPvlt";
            this.grpbxFourPvlt.Size = new System.Drawing.Size(290, 52);
            this.grpbxFourPvlt.TabIndex = 3;
            this.grpbxFourPvlt.TabStop = false;
            this.grpbxFourPvlt.Text = "Menu";
            // 
            // btnResetPvlt
            // 
            this.btnResetPvlt.Location = new System.Drawing.Point(64, 16);
            this.btnResetPvlt.Name = "btnResetPvlt";
            this.btnResetPvlt.Size = new System.Drawing.Size(51, 23);
            this.btnResetPvlt.TabIndex = 3;
            this.btnResetPvlt.Text = "Reset";
            this.btnResetPvlt.UseVisualStyleBackColor = true;
            this.btnResetPvlt.Click += new System.EventHandler(this.btnResetPvlt_Click);
            // 
            // grpbxFivePvlt
            // 
            this.grpbxFivePvlt.Controls.Add(this.lblCheatCodePvlt);
            this.grpbxFivePvlt.Controls.Add(this.lblCheatCodeValuePvlt);
            this.grpbxFivePvlt.Location = new System.Drawing.Point(13, 265);
            this.grpbxFivePvlt.Name = "grpbxFivePvlt";
            this.grpbxFivePvlt.Size = new System.Drawing.Size(290, 52);
            this.grpbxFivePvlt.TabIndex = 4;
            this.grpbxFivePvlt.TabStop = false;
            this.grpbxFivePvlt.Text = "Cheat";
            // 
            // lblCheatCodePvlt
            // 
            this.lblCheatCodePvlt.AutoSize = true;
            this.lblCheatCodePvlt.Location = new System.Drawing.Point(7, 26);
            this.lblCheatCodePvlt.Name = "lblCheatCodePvlt";
            this.lblCheatCodePvlt.Size = new System.Drawing.Size(35, 13);
            this.lblCheatCodePvlt.TabIndex = 1;
            this.lblCheatCodePvlt.Text = "Code:";
            // 
            // lblCheatCodeValuePvlt
            // 
            this.lblCheatCodeValuePvlt.AutoSize = true;
            this.lblCheatCodeValuePvlt.Location = new System.Drawing.Point(54, 27);
            this.lblCheatCodeValuePvlt.Name = "lblCheatCodeValuePvlt";
            this.lblCheatCodeValuePvlt.Size = new System.Drawing.Size(16, 13);
            this.lblCheatCodeValuePvlt.TabIndex = 0;
            this.lblCheatCodeValuePvlt.Text = "...";
            // 
            // grpbxSixPvlt
            // 
            this.grpbxSixPvlt.Controls.Add(this.label1);
            this.grpbxSixPvlt.Controls.Add(this.lblTimerPvlt);
            this.grpbxSixPvlt.Controls.Add(this.lblButtonsPressedPvlt);
            this.grpbxSixPvlt.Controls.Add(this.lblButtonsPressedValuePvlt);
            this.grpbxSixPvlt.Location = new System.Drawing.Point(13, 91);
            this.grpbxSixPvlt.Name = "grpbxSixPvlt";
            this.grpbxSixPvlt.Size = new System.Drawing.Size(290, 52);
            this.grpbxSixPvlt.TabIndex = 5;
            this.grpbxSixPvlt.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(172, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Timer:";
            // 
            // lblTimerPvlt
            // 
            this.lblTimerPvlt.AutoSize = true;
            this.lblTimerPvlt.Location = new System.Drawing.Point(264, 25);
            this.lblTimerPvlt.Name = "lblTimerPvlt";
            this.lblTimerPvlt.Size = new System.Drawing.Size(16, 13);
            this.lblTimerPvlt.TabIndex = 2;
            this.lblTimerPvlt.Text = "...";
            // 
            // lblButtonsPressedPvlt
            // 
            this.lblButtonsPressedPvlt.AutoSize = true;
            this.lblButtonsPressedPvlt.Location = new System.Drawing.Point(11, 25);
            this.lblButtonsPressedPvlt.Name = "lblButtonsPressedPvlt";
            this.lblButtonsPressedPvlt.Size = new System.Drawing.Size(86, 13);
            this.lblButtonsPressedPvlt.TabIndex = 1;
            this.lblButtonsPressedPvlt.Text = "Buttons pressed:";
            // 
            // lblButtonsPressedValuePvlt
            // 
            this.lblButtonsPressedValuePvlt.AutoSize = true;
            this.lblButtonsPressedValuePvlt.Location = new System.Drawing.Point(103, 25);
            this.lblButtonsPressedValuePvlt.Name = "lblButtonsPressedValuePvlt";
            this.lblButtonsPressedValuePvlt.Size = new System.Drawing.Size(16, 13);
            this.lblButtonsPressedValuePvlt.TabIndex = 0;
            this.lblButtonsPressedValuePvlt.Text = "...";
            // 
            // tmrTimerPvlt
            // 
            this.tmrTimerPvlt.Interval = 1000;
            this.tmrTimerPvlt.Tick += new System.EventHandler(this.tmrTimerPvlt_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(319, 267);
            this.Controls.Add(this.grpbxSixPvlt);
            this.Controls.Add(this.grpbxFivePvlt);
            this.Controls.Add(this.grpbxFourPvlt);
            this.Controls.Add(this.grpbxThreePvlt);
            this.Controls.Add(this.grpbxTwoPvlt);
            this.Controls.Add(this.grpbxOnePvlt);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Vault";
            this.grpbxOnePvlt.ResumeLayout(false);
            this.grpbxTwoPvlt.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbxGreenPvlt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxRedPvlt)).EndInit();
            this.grpbxThreePvlt.ResumeLayout(false);
            this.grpbxThreePvlt.PerformLayout();
            this.grpbxFourPvlt.ResumeLayout(false);
            this.grpbxFivePvlt.ResumeLayout(false);
            this.grpbxFivePvlt.PerformLayout();
            this.grpbxSixPvlt.ResumeLayout(false);
            this.grpbxSixPvlt.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpbxOnePvlt;
        private System.Windows.Forms.Button btnThreePvlt;
        private System.Windows.Forms.Button btnTwoPvlt;
        private System.Windows.Forms.Button btnOnePvlt;
        private System.Windows.Forms.GroupBox grpbxTwoPvlt;
        private System.Windows.Forms.PictureBox pbxGreenPvlt;
        private System.Windows.Forms.PictureBox pbxRedPvlt;
        private System.Windows.Forms.GroupBox grpbxThreePvlt;
        private System.Windows.Forms.CheckBox chbxCheckboxPvlt;
        private System.Windows.Forms.Button btnAboutPvlt;
        private System.Windows.Forms.CheckBox chbxSerialPvlt;
        private System.Windows.Forms.GroupBox grpbxFourPvlt;
        private System.Windows.Forms.Button btnResetPvlt;
        private System.Windows.Forms.GroupBox grpbxFivePvlt;
        private System.Windows.Forms.Label lblCheatCodeValuePvlt;
        private System.Windows.Forms.Label lblCheatCodePvlt;
        private System.Windows.Forms.CheckBox ckbCheatPvlt;
        private System.Windows.Forms.GroupBox grpbxSixPvlt;
        private System.Windows.Forms.Label lblButtonsPressedPvlt;
        private System.Windows.Forms.Label lblButtonsPressedValuePvlt;
        private System.Windows.Forms.Timer tmrTimerPvlt;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblTimerPvlt;
    }
}

